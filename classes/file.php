<?php
class files
{
	var $name;	//nombre del archivo .txt
	var $message;	//variable para concatenar los mensajes que arrojan las validaciones y pueda ser mostrada en el navegador.
	var $folder = "txt";	//carpeta dentro de la carpeta intraway para guardar los txt	
	var $lineas = array();
	var $nameNew;
	
	function load()
	{
		$extension = "";
		$name = $this->name;
		$explode = explode('.',$name);
		$extension = end($explode);
		$this->nameNew = time().".".$extension; 
		//Se le cambia el nombre para que si ya existe un archivo igual en la carpeta no la sobreescriba 
		
		if (!file_exists("$this->folder"))	$this->message.="<br>El directorio destino 'txt' no existe, por favor verifique.";
		else
			if (!move_uploaded_file($this->tmp,"$this->folder/".$this->nameNew)) $this->message.="<br>Error al intentar subir archivo txt a la carpeta.";
		
		return $this->message;
	}

	function open()
	{
		$fp = fopen("$this->folder/".$this->nameNew, "r")or exit("Error al abrir archivo!");
		return $fp;
	}
	
	function close($fp)
	{
		$fp = fclose($fp);
		return $fp;
	}
	
	function consume($fp)
	{
		$error = 0;
		$contador = 0;
		$valor = "";
		
		//Lee el archivo TXT, lo guarda en el vector $lineas para manipular sus datos desde cualquier clase
		while(!feof($fp)) {
			
			$valor = trim(fgets($fp)); //se toma el valor de la linea y se le quita los espacios en blanco al inicio y al finl para evitar errores
				
			if ($valor!="")	//para no guardar lineas vacias
			{
				$this->lineas[$contador] = $valor;
				$contador++;
			}
		}
		fseek($fp,0);
		
		return $contador;
	}
	
}
?>