<?php
class subsequences
{
	var $posiciones;
	var $combinaciones;
	var $sequence;
	var $weight;
	var $mayor;
	
	function subsequences()
	{
	}
	
	/*** INICIALIZAR VECTORES POSICIONES Y COMBINACIONES ***/
	function inicializarPosiciones($N)
	{
		for ($i=0; $i<$N; $i++)
			$this->posiciones[$i] = $i;
			//Crear un vector del tamaño de N, en el cual se va a guardar la posicion original de los numeros para tomarlos en cuenta luego de ordenar el vector ascedentemente
	}
	
	function inicializarCombinaciones($count)
	{
		$this->combinaciones = (array_fill(0,$count,0));
		//Crear un vector del tamaño de la longitud de la subsecuencia con valor cero, en el cual se va a guardar todos las combinaciones posibles por subsecuencia
	}
	/*** INICIALIZAR VECTOR POSICIONES  Y COMBINACIONES  ***/
	
	
	/*** ORDENAR VECTORES ***/
	function OrdenarSecuencias($N,$secuencias,$subsecuencias)
	{
		$this->sequence = explode(" ",$secuencias); //crear el vector "sequence" con los valores de la linea secuencia extraida del txt
		$this->weight = explode(" ",$subsecuencias); //crear el vector "weight" con los valores de la linea pesos extraida del txt

		//Ordenamiento Burbuja de forma ascendente en los tres vectores: secuencias, posiciones y pesos
		for ($i=0; $i<$N-1; $i++)
		{
		   for ($j=$i+1; $j<$N; $j++)
		  {
			if(($this->sequence[$i]>$this->sequence[$j])and($this->sequence[$i]>$this->sequence[$j]))
			{	//vector de secuencias
				$aux = $this->sequence[$i];
				$this->sequence[$i] = $this->sequence[$j];
				$this->sequence[$j] = $aux;
				//vector de posiciones
				$aux = $this->posiciones[$i];
				$this->posiciones[$i] = $this->posiciones[$j];
				$this->posiciones[$j] = $aux;
				//vector de valores
				$aux =  $this->weight[$i];
				$this->weight[$i] = $this->weight[$j];
				$this->weight[$j] = $aux;
			}
		  }
		}
	}
	/*** ORDENAR VECTORES ***/
	
	function SumarPesos($puntero,$P,$W)
	{
		$anterior = $puntero[0];
		$suma = $W[$anterior];
		for ($c=1; $c<count($puntero); $c++)
		{
			$posicion1 = $puntero[$c];

			if($P[$anterior]<$P[$posicion1])
			{
				$suma = $suma + $W[$posicion1];
				$anterior = $posicion1;
			}
			else
			{	 
				$suma = 0;
				break;
				//Si dentro de la secuencia encontrada ya un numero no cumple con la condicion, se reincia el valor de suma y se rompe el ciclo para que no siga realizando comparaciones y busqueda
			}
		}
		if ($suma > $this->mayor) //Comparar el resultado de suma con el peso encontrado como mayor
			$this->mayor = $suma;
		
		return $this->mayor;
	}
	
	/*** BUSCAR SECUENCIA MAYOR ***/
	function BuscarCombinacionMayorWeight($N,$longSubsequences,$timesSubsequence,$combinaciones,$P,$W)
	{
		$mayor = 0;
		$cantSec = $timesSubsequence; //Numero de Subsecuencias
		$cantNumSec = $longSubsequences; //Cantidad de numeros en la Subsecuencia
		$combinaciones = $this->combinaciones; //Asignacion del vector combinacion en una variable local de la funcion 
		
		do
		{
			$suma = 0;	
			$movimiento = false;
			for ($i=$cantNumSec-1; $i>=0; $i--) //un for desde la cantidad de numeros de la secuencia hasta cero
			{
				//El vector combinaciones inicialmente esta en ceros, pero manejará valores por posiciones de tantas secuencias haya. Ejemplo: en una secuencia "1 1 2 2 3 3 4 4", el vector combinacion por cada posicion va a guardar los numeros ó 0 ó 1, que son las posibles por cada numero (solo dos veces)
				if (($combinaciones[$i]<$cantSec-1)and($movimiento==false))
				{
					$movimiento = true; //Si ya hizo un movimiento se sale y evalua el peso y las otras condiciones de esta combinacion
					$combinaciones[$i] = $combinaciones[$i] + 1;
					break;
				}
				else
				{
					if (($combinaciones[$i] == $cantSec-1)and($movimiento==false)) //Si es menor al numero de Subsecuencias y no se ha encontrado, continua para buscar otra posicion
						$combinaciones[$i] = 0;
				}
			}
			
			//var_dump($combinaciones);
			$pos = 0;
			
			$sumCombinaciones = 0; 
			for ($j=0;$j<count($combinaciones);$j++)
			{
				//Para el ejemplo de la secuencia "1 1 2 2 3 3 4 4", el vector Combinaciones puede venir "0 1 0 0" que quiere decir que esta combinacion tomó el primer 1, el segundo 2, el primer 3, y el primer 4, basado en las posiciones 0 y 1 por cada numero.  Ahora se debe pasar esa posicion a la equivalente en su posicion dentro del vector combinaciones y se pasa al vector puntero, siguiendo en este ejemplo, el equivalente seria puntero = "0 3 4 6", resultando de tomar la posicion 1 y 0 encontrada y sunarlo al resultado de la multiplicacion del numero de subsecuencias y la posicion dentro del vector (explicacion en el grafico del documento en word). Se toma un contador interno del vector puntero, ya que solo debe moverse si entra en esta condicion y con ello se autodefine su tamaño
				$sumCombinaciones = $sumCombinaciones + $combinaciones[$j];
				$posicionReal = ($j * $cantSec) + $combinaciones[$j];
				$puntero[$pos] = $posicionReal;
				$pos++;
			}
			
			//LLamado al Metodo dentro de esta misma clase para buscar el Peso Mayor
			$mayor = $this->SumarPesos($puntero,$P,$W);
				
		}while($sumCombinaciones!=0); 
		//Este ciclo se hará tantaas veces como combinaciones se cree, hasta que la combinacion vuelva a ser 0,0,0.. es decir la posicion inicial
		return $mayor;
	}
	/*** BUSCAR SECUENCIA MAYOR ***/
	
}
?>