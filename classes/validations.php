<?php
class validations
{
	var $message;	//Variable que guarda los mensajes de error a mostrar
	var $N;			//Longitud de la secuencia
	var $T;			//Numero de secuencias en el txt
	
	function validations()
	{
	}
	
	function validateCharacters($permitidos,$fp)
	{
		$error = 0;
		
		//Lee el archivo TXT, lo guarda en la variable $lineas y valida que no tenga ningun caracter extra
		while(!feof($fp)) {
			$linea = trim(fgets($fp));
			if ($linea!="")
			{
			   //Compruebo que los caracteres en la linea sean los permitidos 
			   for ($i=0; $i<strlen($linea); $i++){ 
				  if (strpos($permitidos, substr($linea,$i,1))===false)
						$error = 1;
			   }
			}
		}
		fseek($fp,0); //Reubicar en la primera linea al puntero del txt
		return $error;
	}
	
	function validateConstraints($lineas,$contador)
	{
		$message = "";
		$error = 0;

		$this->T = $lineas[0]; //Obtener la linea 1, que tiene el numero de ciclos (T)
		
		if (($this->T<1)or($this->T>5)) 
		{	$error = 1;
			$message.= "<br>El numero de ciclos debe ser entre 1 y 5";
		}
		
		if (($this->T*3)!=($contador-1))	//Validar que el numero de ciclos coincida con el numero de lineas		
		{	$error = 1;
			$message.= "<br>El numero de ciclos ingresado en la linea 1 no coincide con los numeros de lineas";
		}

		if ($error==0)
		{
			$posicion = 1; //Variable que va a mover las lineas y empieza desde 1, poque 0 es el numero de ciclos
			for($j=0; $j<$this->T; $j++)
			{
				$this->N = $lineas[$posicion++];	//longitud de la Secuencia (N)
				if (($this->N<1)or($this->N>150000)or(strpos($this->N," ")!=""))//strpos si es mayor de vacio es porque consiguio un espacio que indica que hay mas de un numero en la linea
				{	$error = 1;
					$message.= "<br>El numero de  elementos de la secuencia debe ser un solo numero entero comprendido entre 0 y 150000";
				}
				
				$secuencia = $lineas[$posicion++]; //la secuencia
				if (((strpos($secuencia," ")=="")and($this->N>1))or((strpos($secuencia," ")!="")and($this->N==1)))
				//Valida que si N es mayor que "uno" entonces debe haber espacios entre cada numero, o si N es igual a 1 entonces no debe encontrar espacios en blanco
				{	$error = 1;
					$message.= "<br>Hay espacios en blanco no validos o por el contrario no tiene espacios en blanco y debe tener";
				}
				else
				{
					$a = explode(" ",$secuencia); //Convertir en vector la secuencia de la linea
					if (count($a)!=$this->N)  //Contar si la longitud de la secuencia es igual al valor dado en la linea anterior (N: longitud de la secuencia)
					{	$error = 1; 
						$message.= "<br>La longitud de la secuencia ingresada no coincide con la longitud definida para la secuencia";
					}
				}
					
				$pesos = $lineas[$posicion++];
				//Valida que si la longitud de la secuencia es mayo
				if (((strpos($pesos," ")=="")and($this->N>1))or((strpos($pesos," ")!="")and($this->N==1)))
				{	$error = 1; 
					$message.= "<br>Hay espacios en blanco no validos o por el contrario no tiene espacios en blanco y debe tener";
				}
				else
				{	$w = explode(" ",$pesos); //Convertir en vector el peso de la linea
					if (count($w)!=$this->N) //Contar si la longitud de los pesos es igual al valor dado en la linea de la longitud de la secuencia
					{	$error = 1; 
						$message.= "La longitud de los pesos ingresada no coincide con la longitud definida para la secuencia";
					}
				}
			}
			
		}
		return array($error,$message);
		//Devuelve error (0:correcto, 1:incorrecto) y la varible mensajes la lista de los errores encontrados

	}
}
?>