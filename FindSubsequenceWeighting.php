<?php


	include("classes/file.php");
	include("classes/validations.php");
	include("classes/subsequences.php");

	//Instanciar las tres clases creadas
	$file = new files();	//Clase  de los metodos del archivo abrir, cerrar, cargar y consumir
	$validations = new validations();	//Clase para validar las lineas, controlar la aparicion de caracteres y los constraints dados en el enunciado
	$subsequences = new subsequences();	//Clase para manipular las subsecuencias y sumar sus pesos
	
	//Recibir archivo txt
	$file->name = $_FILES["filetxt"]["name"]; 
	$file->tmp = $_FILES["filetxt"]["tmp_name"];
	$validate = false;
	
	//Upload del archivo txt
	$message = $file->load();
	
	if ($message=="")
	{
		//abrir el archivo
		$fp = $file->open();
		
		//Validar que el archivo solo contenga numeros y espacios en blancos
		$result = $validations->validateCharacters("1234567890 ",$fp); //para que sea reutlizable, se envia por parametro los caracteres que se quieren validar
		
		if ($result==1) echo "<br>El contenido tiene caracteres no validos!!"; 
		else
		if ($result==0)	//Contenido correcto, continuamos
		{
			$contador = $file->consume($fp); //Metodo que carga el contenido de las lineas del archivo txt en un vector llamado lineas
			
			if ($result==1) echo "<br>El contenido no pudo ser consumido!!"; 
			else
			if ($result==0)	//Contenido consumido, continuamos
			{
				/*	Llamado al Metodo que valida los Constrains del enunciado para el correcto funcionamento de las combianciones.
					Devuelve dos variables, error como switch para saber con 0 que no hay error y si es 1 si hay error y se imprime en el div los mensajes de la(s) causa(s) del error que han sido concatenada en la variable $message que devuelve el metodo.
				*/
				list($error,$message) = $validations->validateConstraints($file->lineas,$contador);
				if ($error!=0) echo "Error: ".$error." Mensaje: ".$message;
				else	$validate = true;
			}
		}
		
		//Si validate == true, ha cumplido las condiciones de los Constrains y se procede a las operaciones de conseguir las combinaciones
		if ($validate==true)
		{
			$puntero = 0;
			
			for ($i=1; $i<=$validations->T; $i++)//Se realiza el proceso por tantas secuencias enuncie la linea 1 del archivo txt
			{
				$count = 0;
				$times = 0;  //Variable que guarda el numero de subsecuencias dentro de la secuencia
				$puntero = $puntero + 1;  //Variable para manipular la posicion dentro del vector linea (contiene las lineas del txt)
				$vectorAux = array();
				$subsequences->mayor = 0; //Variable para almacenar la suma de los pesos encontrada como mayor
				$N = $file->lineas[$puntero++];
				$secuencias = $file->lineas[$puntero++]; //El metodo lineas, devuelve el vector con las linea del txt, en la posicion puntero
				$subsecuencias = $file->lineas[$puntero];
				$subsequences->combinaciones = "";
				$subsequences->inicializarPosiciones($N); //Inicializar vector de posiciones actual de los numeros en la secuencia
				$subsequences->OrdenarSecuencias($N,$secuencias,$subsecuencias);
/* Ordenar los 3 vectores de manera ascendente (a partir de este momento la secuencia que por ejemplo era 1 2 3 4 1 2 3 4, se convierte en 1 1 2 2 3 3 4 4, habiendo guardado en el vector 'Posiciones' la posicion original de cada numero dentro de la secuencia */

				//Conseguir cantidad de Subsecuencias en la secuencia
				$first = $subsequences->sequence[0];
				for ($m=0;$m<count($subsequences->sequence);$m++)
					if ($subsequences->sequence[$m]==$first)
						$times++;//Numero de Subsecuencias, al conseguir cuantas veces se encuentre el primer numero
				
				//Inicializar vector de combinaciones para guardar en el las combinaciones posibles
				$clearSequence = array_unique($subsequences->sequence);
				$longSubsequences = count($clearSequence); //Cantidad de Numeros en la Secuencia
				$subsequences->inicializarCombinaciones($longSubsequences); //Inicializa el vector en ceros, para guardar las combinaciones
				
				//Si solo tiene una subsecuencia la secuencia
				if ($times==1)
				{
					for ($l=0; $l<$longSubsequences; $l++)
						$vectorAux[$l] = $l; //vector posiciones
												
					//sumar los pesos de la secuencia, por ser una sola automaticamente se convierte en el peso mayor
					$mayor = $subsequences->SumarPesos($vectorAux,$subsequences->posiciones,$subsequences->weight); 
				}
				else //Si encuentra mas de una subsecuencia, se procede a realizar las operaciones para conseguir el peso mayor de esta secuencia
					$mayor = $subsequences->BuscarCombinacionMayorWeight
					($N,$longSubsequences,$times,$subsequences->combinaciones,$subsequences->posiciones,$subsequences->weight);
				
				//Imprimir resultado
				echo "<p>".$mayor."</p>";
			}
			
		}
	}
?>