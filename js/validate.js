/* Validacion y envio del formulario a traves de jquery en el formulario index.html */
$(function(){
	$("#form").on("submit", function(e){
		e.preventDefault();
		var f = $(this);
		var formData = new FormData(document.getElementById("form"));
		var filename = $("#filetxt").val();
		var extension = filename.replace(/^.*\./, '');
		
		if (extension == filename) extension = '';
       	else	extension = extension.toLowerCase(); //convierte en minuscula la extension
		
		if (extension!="txt")
		{ 
			$("#result").empty();
			$("#result").append("<span style='color:#D80000'>Extension invalida! Intente de nuevo, el archivo debe ser un documento .txt</span>");
		}
		else
		{
			formData.append("dato", "valor");
			$.ajax({
				url: "FindSubsequenceWeighting.php",
				type: "post",
				dataType: "html",
				data: formData,
				cache: false,
				contentType: false,
				processData: false
			})
			.done(function(res){
				$("#result").html("" + res);
			});
		}
	});
});